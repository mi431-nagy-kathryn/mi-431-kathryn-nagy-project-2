using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    public SpriteGenerator sg;
    public string exportPath = "TestProceduralTextureFolder";

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(Application.dataPath);
        sg.GenerateTextureTopLevel();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

/*
[CustomEditor(typeof(TestScript))]
public class TestScriptEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var spriteGenerator = target as SpriteGenerator;

        if (GUILayout.Button("Generate Tiles"))
        {
            Debug.Log("Generate Tiles button clicked");
        }
    }
}*/