using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using System.IO;
using UnityEditor;

[CreateAssetMenu(fileName = "SpriteGenerator")]
public class SpriteGenerator : ScriptableObject
{
    public Tile tileToModify;
    public Texture2D baseTexture;
    public Texture2D simplifiedTexture1;
    public Texture2D simplifiedTexture2;
    public Texture2D singleTexture1;
    public Texture2D singleTexture2;
    public Texture2D adjacentTexture1;
    public Texture2D adjacentTexture2;
    public Texture2D skipTexture1;
    public Texture2D skipTexture2;
    public Texture2D oppositeTexture1;
    public Texture2D oppositeTexture2;
    public Texture2D multiplyOverlayTexture;

    public Color baseColor;
    public Color topLayerColor;

    public bool[] sides = new bool[6];

    [Tooltip("If this is enabled, then simplified textures are generated, overriding the differences in the following options.")]
    public bool useSimplifiedTextures = false;
    [Tooltip("If this is enabled, two adjacent sides that have the top layer enabled will be visually merged together.")]
    public bool mergeAdjacent = true;
    [Tooltip("If this is enabled, two sides that have the top layer enabled, but are separated by 1 side, will be visually merged together in all circumstances.")]
    public bool mergeSkipAlways = false;
    [Tooltip("If this is enabled, two sides that have the top layer enabled, but are separated by 1 side, will be visually merged together when there are both adjacent and opposite sides already being merged, regardless of whether mergeSkipAlways is true.")]
    public bool mergeSkipToJoinMultiple = true;
    [Tooltip("If this is enabled, two opposites sides that have the top layer enabled will be visually merged together.")]
    public bool mergeOpposite = true;
    [Tooltip("This is used to adjust visual glitches, in which part of the tile was incorrectly cropped.")]
    public int widthOffset = 5;

    public string exportPath = "TestProceduralTextureFolder";
    public char baseLayerCharacter = '0';
    public char topLayerCharacter = '1';

    public int pixelsPerUnit = 256;

    private Sprite mySprite;
    private SpriteRenderer sr;
    Texture2D[] textures = new Texture2D[64];
    Tile[] tiles = new Tile[64];

    Vector2Int textureSize = new Vector2Int(0, 0);
    Texture2D exportedTexture;

    enum Offsets { Simplified, Single, Adjacent, Skip, Opposite }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void GenerateTextureTopLevel()
    {
        //sr = GetComponent<SpriteRenderer>();
        exportedTexture = new Texture2D(0, 0);
        textureSize = new Vector2Int(baseTexture.width, baseTexture.height);
        exportedTexture.Reinitialize(textureSize.x, textureSize.y);

        InitializeFolderStructure();

        for (int i = 0; i < 64; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                sides[j] = ((i >> j) % 2 == 1);
            }
            GenerateExportedTexture();

            byte[] bytes = exportedTexture.EncodeToPNG();

            string imageID = "";
            for (int j = 0; j < 6; ++j) 
            {
                imageID += sides[j] ? topLayerCharacter : baseLayerCharacter;
            }

            string filepath = "Assets/Resources/" + exportPath + "/Textures/" + imageID + ".png";
            File.WriteAllBytes(filepath, bytes);
        }

        for (int i = 0; i < 64; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                sides[j] = ((i >> j) % 2 == 1);
            }

            string imageID = "";
            for (int j = 0; j < 6; ++j)
            {
                imageID += sides[j] ? topLayerCharacter : baseLayerCharacter;
            }

            textures[i] = new Texture2D(textureSize.x, textureSize.y);
            textures[i] = Resources.Load<Texture2D>(exportPath + "/Textures/" + imageID);

            if (textures[i] == null) Debug.Log("Invalid texture ID " + imageID);

            Sprite tileSprite = Sprite.Create(textures[i], new Rect(0, 0, textureSize.x, textureSize.y), Vector2.one * 0.5f, pixelsPerUnit);
            AssetDatabase.CreateAsset(tileSprite, "Assets/Resources/" + exportPath + "/Sprites/" + imageID + ".asset");
        }

        for (int i = 0; i < 64; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                sides[j] = ((i >> j) % 2 == 1);
            }

            string imageID = "";
            for (int j = 0; j < 6; ++j)
            {
                imageID += sides[j] ? topLayerCharacter : baseLayerCharacter;
            }

            tiles[i] = new Tile();
            tiles[i].sprite = Resources.Load<Sprite>(exportPath + "/Sprites/" + imageID);

            AssetDatabase.CreateAsset(tiles[i], "Assets/Resources/" + exportPath + "/Tiles/" + "Tile" + imageID + ".asset");
        }
        //Sprite sprite = Sprite.Create(exportedTexture, new Rect(0.0f, 0.0f, textureSize.x, textureSize.y), new Vector2(0.5f, 0.5f), pixelsPerUnit);
        //sr.sprite = sprite;
    }

    public void InitializeFolderStructure()
    {
        /*
         * Assets/Resources
         * Assets/Resources/exportPath
         * Assets/Resources/exportPath/Sprites
         * Assets/Resources/exportPath/Tiles
        */
        if (!AssetDatabase.IsValidFolder("Assets/Resources"))
        {
            AssetDatabase.CreateFolder("Assets", "Resources");
        }

        if (!AssetDatabase.IsValidFolder("Assets/Resources/" + exportPath))
        {
            AssetDatabase.CreateFolder("Assets/Resources", exportPath);
        }

        if (!AssetDatabase.IsValidFolder("Assets/Resources/" + exportPath + "/Textures"))
        {
            AssetDatabase.CreateFolder("Assets/Resources/" + exportPath, "Textures");
        }

        if (!AssetDatabase.IsValidFolder("Assets/Resources/" + exportPath + "/Sprites"))
        {
            AssetDatabase.CreateFolder("Assets/Resources/" + exportPath, "Sprites");
        }

        if (!AssetDatabase.IsValidFolder("Assets/Resources/" + exportPath + "/Tiles"))
        {
            AssetDatabase.CreateFolder("Assets/Resources/" + exportPath, "Tiles");
        }

        if (!AssetDatabase.IsValidFolder("Assets/Resources/" + exportPath + "/TileObjectSOs"))
        {
            AssetDatabase.CreateFolder("Assets/Resources/" + exportPath, "TileObjectSOs");
        }
    }

    public void GenerateTiles()
    {
        for (int i = 0; i < 64; i++)
        {
            bool[] tempSides = new bool[6];
            for (int j = 0; j < 6; j++)
            {
                tempSides[j] = ((i >> j) % 2 == 1);
            }
        }

        string imageID = "";
        for (int j = 0; j < 6; ++j)
        {
            imageID += sides[j] ? topLayerCharacter : baseLayerCharacter;
        }

        Tile tile = new Tile();
        var testResource = Resources.Load("Assets/" + exportPath + "/Sprites/" + imageID + ".png");
        tile.sprite = testResource as Sprite;
        AssetDatabase.CreateAsset(tile, "Assets/" + exportPath + "/Tiles/" + "Tile" + imageID + ".asset");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void GenerateExportedTexture()
    {
        for (int x = 0; x < textureSize.x; x++) 
        {
            for (int y = 0; y < textureSize.y; y++)
            {
                exportedTexture.SetPixel(x, y, baseTexture.GetPixel(x, y));
            }
        }

        bool useTexture2 = false;
        bool flipX = false;
        bool flipY = false;

        bool adjacentMerged = false;
        bool oppositeMerged = false;

        for (int i = 0; i < sides.Length; i++)
        {
            
            if (sides[i])
            {
                setRenderValues(i, useSimplifiedTextures? Offsets.Simplified : Offsets.Single, ref useTexture2, ref flipX, ref flipY);
                Texture2D thisLayer = textureLayerToAdd(useTexture2, useSimplifiedTextures ? Offsets.Simplified : Offsets.Single);
                for (int x = 0; x < textureSize.x; x++)
                {
                    for (int y = 0; y < textureSize.y; y++)
                    {
                        int xCoord = flipX ? (textureSize.x - x) - 1 : x;
                        int yCoord = flipY ? (textureSize.y - y) - 1 : y;
                        exportedTexture.SetPixel(x, y, Color.Lerp(exportedTexture.GetPixel(x, y), thisLayer.GetPixel(xCoord, yCoord), thisLayer.GetPixel(xCoord, yCoord).a));
                    }
                }
            }

            if (!useSimplifiedTextures)
            {
                if (sides[i] && sides[(i + 1) % 6] && mergeAdjacent)
                {
                    setRenderValues(i, Offsets.Adjacent, ref useTexture2, ref flipX, ref flipY);
                    Texture2D thisLayer = textureLayerToAdd(useTexture2, Offsets.Adjacent);
                    for (int x = 0; x < textureSize.x; x++)
                    {
                        for (int y = 0; y < textureSize.y; y++)
                        {
                            int xCoord = flipX ? (textureSize.x - x) - 1 : x;
                            int yCoord = flipY ? (textureSize.y - y) - 1 : y;
                            exportedTexture.SetPixel(x, y, Color.Lerp(exportedTexture.GetPixel(x, y), thisLayer.GetPixel(xCoord, yCoord), thisLayer.GetPixel(xCoord, yCoord).a));
                        }
                    }
                    adjacentMerged = true;
                }



                if (sides[i] && sides[(i + 3) % 6] && mergeOpposite)
                {
                    setRenderValues(i, Offsets.Opposite, ref useTexture2, ref flipX, ref flipY);
                    Texture2D thisLayer = textureLayerToAdd(useTexture2, Offsets.Opposite);
                    for (int x = 0; x < textureSize.x; x++)
                    {
                        for (int y = 0; y < textureSize.y; y++)
                        {
                            int xCoord = flipX ? (textureSize.x - x) - 1 : x;
                            int yCoord = flipY ? (textureSize.y - y) - 1 : y;
                            exportedTexture.SetPixel(x, y, Color.Lerp(exportedTexture.GetPixel(x, y), thisLayer.GetPixel(xCoord, yCoord), thisLayer.GetPixel(xCoord, yCoord).a));
                        }
                    }
                    oppositeMerged = true;
                }
            }
            

            
        }

        if (!useSimplifiedTextures)
        {
            for (int i = 0; i < sides.Length; i++)
            {
                //Apply this afterwards - skipped sprites are added in when both opposite and adjacent are present, even if mergeSkip is disabled, for improved appearance
                if (sides[i] && sides[(i + 2) % 6] && (mergeSkipAlways || (oppositeMerged && adjacentMerged && mergeSkipToJoinMultiple)))
                {
                    setRenderValues(i, Offsets.Skip, ref useTexture2, ref flipX, ref flipY);
                    Texture2D thisLayer = textureLayerToAdd(useTexture2, Offsets.Skip);
                    for (int x = 0; x < textureSize.x; x++)
                    {
                        for (int y = 0; y < textureSize.y; y++)
                        {
                            int xCoord = flipX ? (textureSize.x - x) - 1 : x;
                            int yCoord = flipY ? (textureSize.y - y) - 1 : y;
                            exportedTexture.SetPixel(x, y, Color.Lerp(exportedTexture.GetPixel(x, y), thisLayer.GetPixel(xCoord, yCoord), thisLayer.GetPixel(xCoord, yCoord).a));
                        }
                    }
                }
            }
        }

        //Color lerping goes here
        for (int x = 0; x < textureSize.x; x++)
        {
            for (int y = 0; y < textureSize.y; y++)
            {
                exportedTexture.SetPixel(x, y, Color.Lerp(topLayerColor, baseColor, exportedTexture.GetPixel(x, y).r));
            }
        }

        //Applying the multiply overlay
        for (int x = 0; x < textureSize.x; x++)
        {
            for (int y = 0; y < textureSize.y; y++)
            {
                exportedTexture.SetPixel(x, y, exportedTexture.GetPixel(x, y) * multiplyOverlayTexture.GetPixel(x, y));
            }
        }


        exportedTexture.Apply();
    }

    Texture2D textureLayerToAdd(bool useSecondaryTextureVariant, Offsets offset = Offsets.Single)
    {
        if (!useSecondaryTextureVariant)
        {
            switch (offset)
            {
                case Offsets.Single:
                    return singleTexture1;
                case Offsets.Adjacent:
                    return adjacentTexture1;
                case Offsets.Skip:
                    return skipTexture1;
                case Offsets.Opposite:
                    return oppositeTexture1;
                default:
                    return simplifiedTexture1;
            }

        }
        else
        {
            switch (offset)
            {
                case Offsets.Single:
                    return singleTexture2;
                case Offsets.Adjacent:
                    return adjacentTexture2;
                case Offsets.Skip:
                    return skipTexture2;
                case Offsets.Opposite:
                    return oppositeTexture2;
                default:
                    return simplifiedTexture2;
            }
        }
    }

    void setRenderValues(int firstIndex, Offsets offset, ref bool useSecondary, ref bool flipX, ref bool flipY)
    {
        switch (offset)
        {
            case Offsets.Simplified:
            case Offsets.Single:
                switch (firstIndex)
                {
                    case 0:
                        useSecondary = false;
                        flipX = false;
                        flipY = false;
                        return;
                    case 1:
                        useSecondary = false;
                        flipX = true;
                        flipY = false;
                        return;
                    case 2:
                        useSecondary = true;
                        flipX = false;
                        flipY = false;
                        return;
                    case 3:
                        useSecondary = false;
                        flipX = true;
                        flipY = true;
                        return;
                    case 4:
                        useSecondary = false;
                        flipX = false;
                        flipY = true;
                        return;
                    case 5:
                        useSecondary = true;
                        flipX = true;
                        flipY = true;
                        return;
                    default:
                        useSecondary = false;
                        flipX = false;
                        flipY = false;
                        return;
                }
            case Offsets.Adjacent:
                switch (firstIndex)
                {
                    case 0:
                        useSecondary = true;
                        flipX = false;
                        flipY = false;
                        return;
                    case 1:
                        useSecondary = false;
                        flipX = false;
                        flipY = false;
                        return;
                    case 2:
                        useSecondary = false;
                        flipX = false;
                        flipY = true;
                        return;
                    case 3:
                        useSecondary = true;
                        flipX = true;
                        flipY = true;
                        return;
                    case 4:
                        useSecondary = false;
                        flipX = true;
                        flipY = true;
                        return;
                    case 5:
                        useSecondary = false;
                        flipX = true;
                        flipY = false;
                        return;
                    default:
                        useSecondary = false;
                        flipX = false;
                        flipY = false;
                        return;
                }
            case Offsets.Skip:
                switch (firstIndex)
                {
                    case 0:
                        useSecondary = false;
                        flipX = false;
                        flipY = false;
                        return;
                    case 1:
                        useSecondary = true;
                        flipX = false;
                        flipY = false;
                        return;
                    case 2:
                        useSecondary = false;
                        flipX = false;
                        flipY = true;
                        return;
                    case 3:
                        useSecondary = false;
                        flipX = true;
                        flipY = false;
                        return;
                    case 4:
                        useSecondary = true;
                        flipX = true;
                        flipY = false;
                        return;
                    case 5:
                        useSecondary = false;
                        flipX = true;
                        flipY = false;
                        return;
                    default:
                        useSecondary = false;
                        flipX = false;
                        flipY = false;
                        return;
                }
            case Offsets.Opposite:
                switch (firstIndex)
                {
                    case 0:
                    case 3:
                        useSecondary = false;
                        flipX = false;
                        flipY = false;
                        return;
                    case 1:
                    case 4:
                        useSecondary = false;
                        flipX = true;
                        flipY = false;
                        return; 
                    case 2:
                    case 5:
                        useSecondary = true;
                        flipX = false;
                        flipY = false;
                        return;
                    default:
                        useSecondary = false;
                        flipX = false;
                        flipY = false;
                        return;
                }
        }
    }
}


[CustomEditor(typeof(SpriteGenerator))]
public class SpriteGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        
        var spriteGenerator = target as SpriteGenerator;

        if (GUILayout.Button("Generate Textures"))
        {
            Debug.Log("Generate Textures button clicked");
            spriteGenerator.GenerateTextureTopLevel();
        }

        /*if (GUILayout.Button("Generate Tile Assets"))
        {
            spriteGenerator.GenerateTiles();
        }*/

        base.OnInspectorGUI();
    }
}