using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "CategorizedProbabilityCalculator")]
public class CategorizedProbabilityCalculator : ProbabilityCalculator
{
    [Serializable]
    public class TileCategory
    {
        public string name;
        public List<Tile> tiles;
    }

    [Serializable]
    public class CategoryWeightPair
    {
        public string name;
        public int weight = -1;
    }
    [Serializable]
    public class CategoryWeightValues
    {
        public string name;
        public List<CategoryWeightPairList> priorityPairLists;
    }

    [Serializable]
    public class CategoryWeightPairList
    {
        public List<CategoryWeightPair> pairs;
    }

    [Serializable]
    public class OverrideName
    {
        public string currentName;
        public string newName;
    }


    public List<TileObject> tileObjects;

    [SerializeField] public List<TileCategory> categories;
    public int possibleRotations = 6;
    public Dictionary<string, TileCategory> tileCategoriesByName = new Dictionary<string, TileCategory>();

    public List<OverrideName> overrideNames = new List<OverrideName>();
    public Dictionary<string, string> overrideNamePairs = new Dictionary<string, string>();

    [Tooltip("Leave a weight as -1 to have this specific entry use the default weight value specified as DefaultWeight")]
    public List<CategoryWeightValues> categoryWeightValues = new List<CategoryWeightValues>();
    public Dictionary<string, List<Dictionary<string, int>>> categoryWeights = new Dictionary<string, List<Dictionary<string, int>>>(); //Top-level is existing tile, second-level is possible neighbor tile, int is weight for probability calculations


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Create a button to call this
    public void GenerateRotationCategories(bool reset = true, string loadPath = "TestProceduralTextureFolder")
    {
        if (reset)
        {
            categories.Clear();
        }

        foreach (TileObject tile in tileObjects)
        {
            bool found = false;
            string numberID = tile.name.Replace("TileObject", "");
            foreach(TileCategory category in categories)
            {
                if (category.tiles.Contains(tile.mapTile))
                {
                    found = true;
                    tileCategoriesByName.Add(numberID, category);  //Ensure correct tilename being used
                }
            }

            if (!found)
            {
                TileCategory tileCategory = new TileCategory();
                tileCategory.name = tile.mapTile.name;
                List<Tile> variations = new List<Tile>();
                //Make sure the actual name is correct when debugging
                string imageID = numberID;
                for (int i = 0; i < possibleRotations; i++)
                {
                    string filepath = loadPath + "/Tiles/Tile" + numberID;
                    Tile tileToAdd = Resources.Load<Tile>(filepath);
                    if (!variations.Contains(tileToAdd)) variations.Add(tileToAdd);
                    numberID = numberID[1..] + numberID[0]; //Rework this to avoid duplicates
                }
                tileCategory.tiles = variations;
                categories.Add(tileCategory);
                tileCategoriesByName[tile.name] = tileCategory;
            }
        }


        //Have option to auto-rename all the categories based on a dictionary going from original name to something more legible specified elsewhere
        foreach (OverrideName namePair in overrideNames) 
        {
            overrideNamePairs.Add(namePair.currentName, namePair.newName);
        }

        foreach (TileCategory category in categories)
        {
            if (overrideNamePairs.ContainsKey(category.name))
            {
                category.name = overrideNamePairs[category.name];
            }
        }

        
        
    }

    public void AssignWeights()
    {
        categoryWeights.Clear();
        probabilityDictionary.Clear();
        //Create the main structure for probabilities
        foreach (CategoryWeightValues weight in categoryWeightValues) 
        {
            List<Dictionary<string, int>> dict = new List<Dictionary<string, int>>();
            foreach (CategoryWeightPairList pairList in weight.priorityPairLists)
            {
                Dictionary<string, int> weightsForThis = new Dictionary<string, int>();
                foreach (CategoryWeightPair pair in pairList.pairs)
                {
                    weightsForThis.Add(pair.name, pair.weight);
                }
                dict.Add(weightsForThis);
            }
            categoryWeights.Add(weight.name, dict);
        }

        //Traverse this dictionary to create necessary tile weight entries
        foreach (TileObject tile in tileObjects)
        {
            //Find the category that this tile belongs to, and get the name of it
            string categoryName = "";
            foreach (TileCategory category in categories)
            {
                if (category.tiles.Contains(tile.mapTile))
                {
                    categoryName = category.name;
                    break;
                }
            }

            if (!categoryWeights.ContainsKey(categoryName)) continue;

            List<List<TileWeightPair>> weightPairList = new List<List<TileWeightPair>>();
            //Iterate over each list separately to maintain priority queues
            foreach (List<TileWeightPair> pair in weightPairList)
            {
                Dictionary<string, int> weightsForThisTile = new Dictionary<string, int>();
                List<TileWeightPair> weightPairs = new List<TileWeightPair>();
                //Iterate over each other possible tile, finding the category it belongs to, and specifying the weight (if it is not -1)
                foreach (TileObject subTile in tileObjects)
                {
                    string secondaryCategoryName = "";
                    foreach (TileCategory secondaryCategory in categories)
                    {
                        if (secondaryCategory.tiles.Contains(subTile.mapTile))
                        {
                            secondaryCategoryName = secondaryCategory.name;
                            break;
                        }
                    }

                    bool secondaryCategoryFound = false;
                    Dictionary<string, int> dictToSearch = new Dictionary<string, int>();
                    foreach (Dictionary<string, int> priorityDict in categoryWeights[categoryName])
                    {
                        if (priorityDict.ContainsKey(secondaryCategoryName)) 
                        {
                            secondaryCategoryFound = true;
                            dictToSearch = priorityDict;
                            break;
                        }
                    }
                    if (!secondaryCategoryFound) continue;
                    int weightToUse = dictToSearch[secondaryCategoryName];

                    if (secondaryCategoryName != "" && weightToUse != -1)
                    {
                        //Iterate over all of the tiles in this secondary category
                        //Create a TileWeightPair consisting of the secondary tile and the weight, then add that to the entry

                        TileWeightPair weightPair = new TileWeightPair();
                        weightPair.tile = subTile.mapTile;
                        weightPair.weight = weightToUse;

                        weightPairs.Add(weightPair);
                    }
                }

                weightPairList.Add(weightPairs);
            }
            probabilityDictionary.Add(tile.mapTile, weightPairList);

        }
        Debug.Log("Entries in probability dictionary: " + probabilityDictionary.Count);
        this.SetDirty();
        AssetDatabase.SaveAssets();
    }
}

[CustomEditor(typeof(CategorizedProbabilityCalculator))]
public class CategorizedProbabilityEditor : Editor
{
    public override void OnInspectorGUI()
    {
        var catProb = target as CategorizedProbabilityCalculator;

        if (GUILayout.Button("Generate Rotation Categories"))
        {
            Debug.Log("Generate Textures button clicked");
            catProb.GenerateRotationCategories();
        }

        if (GUILayout.Button("Create Probability Table"))
        {
            catProb.AssignWeights();
            Debug.Log("Weights assigned");
        }

        base.OnInspectorGUI();
    }
}
