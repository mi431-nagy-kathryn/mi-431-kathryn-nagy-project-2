using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;
using System.IO;
using System.Dynamic;

public class Generator : MonoBehaviour
{
    public String[] exportPaths = {"TestProceduralTextureFolder"};

    [Serializable]
    public class characterEdgePair
    {
        public char key;
        public TileEdge.Edge value;
    }
    public List<characterEdgePair> characterEdgePairs = new List<characterEdgePair>();
    Dictionary<char, TileEdge.Edge> charEdgeDict = new Dictionary<char, TileEdge.Edge>();

    public int iterations = 2;
    public bool constrainToRange = false;
    public Vector2 constraintMin = new Vector2(0, 0);
    public Vector2 constraintMax = new Vector2(20, 20);
    Vector3Int origin = Vector3Int.zero;
    Vector3Int[] offsets = { new Vector3Int(0, 1, 0), new Vector3Int(1, 0, 0), new Vector3Int(1, 0, 0), new Vector3Int(0, -1, 0), new Vector3Int(-1, -1, 0), new Vector3Int(-1, 1, 0) }; //Starts at upper left, goes clockwise, for pointed-top hex tiles.
    Dictionary<Vector3Int, TileState> states = new Dictionary<Vector3Int, TileState>();
    public Tilemap tilemap;
    Dictionary<Tile, List<List<Tile>>> adjacencies = new Dictionary<Tile, List<List<Tile>>>();

    public List<Tile> testingAdjacencyList = new List<Tile>();
    [Serializable]
    public class TileAdjacencies
    {
        public Tile tileType;
        public List<Tile> upperLeftNeighbors;
        public List<Tile> middleLeftNeighbors;
        public List<Tile> lowerLeftNeighbors;
        public List<Tile> upperRightNeighbors;
        public List<Tile> middleRightNeighbors;
        public List<Tile> lowerRightNeighbors;
    }

    public List<RotatableTile> rotatableTiles = new List<RotatableTile>();

    public List<TileObject> masterTileList = new List<TileObject>();

    public List<TileAdjacencies> masterAdjacenciesList = new List<TileAdjacencies>();

    public ProbabilityCalculator probabilityCalculator;

    // Start is called before the first frame update
    void Start()
    {
        /*foreach (TileAdjacencies x in masterAdjacenciesList)
        {
            List<List<Tile>> tiles = new List<List<Tile>>();
            tiles.Add(x.upperLeftNeighbors);
            tiles.Add(x.upperRightNeighbors);
            tiles.Add(x.middleRightNeighbors);
            tiles.Add(x.lowerRightNeighbors);
            tiles.Add(x.lowerLeftNeighbors);
            tiles.Add(x.middleLeftNeighbors);
            adjacencies.Add(x.tileType, tiles);
        }*/
        PopulateTileList();
        Generate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Generate()
    {
        Vector3Int currentPosition = origin;
        states.Add(currentPosition, new TileState());

        List<List<Tile>> testingList = new List<List<Tile>>();
        List<Tile> tempSubList = new List<Tile>();
        foreach (TileObject tileObject in masterTileList)
        {
            tempSubList.Add(tileObject.mapTile);
        }
        testingList.Add(tempSubList);

        List<Vector3Int> previousFrontier = new List<Vector3Int>();
        previousFrontier.Add(origin);

        //Template for Generation
        states[currentPosition].determinePossibleNeighbors(testingList);
        tilemap.SetTile(currentPosition, states[currentPosition].Collapse());

        
        for (int i = 0; i < iterations; i++)
        {
            List<Vector3Int> positionsToGenerate = new List<Vector3Int>();
            //Get the frontier of neighbors that need tile generation
            foreach (Vector3Int generatePosition in previousFrontier)
            {
                foreach (Vector3Int pos in GetOffsets(generatePosition))
                {
                    if (!constrainToRange || isPositionInBounds(generatePosition + pos))
                    {
                        if (!states.ContainsKey(generatePosition + pos))
                        {
                            states.Add(generatePosition + pos, new TileState());
                        }

                        if (!states[generatePosition + pos].isCollapsed)
                        {
                            positionsToGenerate.Add(generatePosition + pos);
                        }
                    }
                }
            }

            //Generation
            foreach (Vector3Int newTile in positionsToGenerate)
            {
                currentPosition = newTile;
                List<List<Tile>> adjacencyListForGeneration = new List<List<Tile>>();

                int offsetIndex = 0;
                foreach (Vector3Int pos in GetOffsets(currentPosition))
                {
                    if (!constrainToRange || isPositionInBounds(currentPosition + pos))
                    {
                        if (states.ContainsKey(currentPosition + pos))
                        {
                            if (states[currentPosition + pos].isCollapsed)
                            {
                                //In the final version, get the adjacency list that corresponds to this location - the opposing offset. Or, pass in this offset to get the right list? How should I structure it?
                                adjacencyListForGeneration.Add(adjacenciesForThisTile(offsetIndex, currentPosition + pos));
                                //adjacencyListForGeneration.Add(testingAdjacencyList);
                            }
                        }
                    }
                    offsetIndex++;
                }

                if (adjacencyListForGeneration.Count == 0)
                {
                    Debug.Log("Encountered a situation with no adjacency list for generation.");
                }
                
                states[currentPosition].determinePossibleNeighbors(adjacencyListForGeneration);
                List<Tile> existingNeighbors = GetExistingNeighbors(currentPosition);
                //Tile newTileToSet = states[currentPosition].Collapse();
                Tile newTileToSet = states[currentPosition].Collapse(existingNeighbors, probabilityCalculator);
                if (newTileToSet != null)
                {
                    tilemap.SetTile(currentPosition, newTileToSet);
                }
                
            }

            previousFrontier = new List<Vector3Int>(positionsToGenerate);
        }
    }

    Vector3Int[] GetOffsets(Vector3Int currentPos)
    {
        if (currentPos.y % 2 != 0)
        {
            return new Vector3Int[] { new Vector3Int(0, 1, 0), new Vector3Int(1, 1, 0), new Vector3Int(1, 0, 0), new Vector3Int(1, -1, 0), new Vector3Int(0, -1 ,0), new Vector3Int(-1, 0, 0)};
        }
        else
        {
            return new Vector3Int[] { new Vector3Int(-1, 1, 0), new Vector3Int(0, 1, 0), new Vector3Int(1, 0, 0), new Vector3Int(0, -1, 0), new Vector3Int(-1, -1, 0), new Vector3Int(-1, 0, 0) };
        }
    }

    //This needs to be changed, but how exactly?
    List<Tile> adjacenciesForThisTile(int index, Vector3Int positionExistingTile)
    {
        Tile tileType = tilemap.GetTile(positionExistingTile) as Tile;//Get the type of the tile at positionExistingTile
        if (tileType != null && adjacencies.ContainsKey(tileType))
        {
            return adjacencies[tileType][(index + 3) % 6];
        }
        else
        {
            Debug.Log("Possible issue with adjacencies - attempted to find adjacencies for a null tile, or a tile not in the adjacencies list.");
            return new List<Tile>();
        }
        

    }

    List<Tile> GetExistingNeighbors(Vector3Int position)
    {
        List<Tile> returnList = new List<Tile>();
        Vector3Int[] positionsToCheck = GetOffsets(position);
        for (int i = 0; i < positionsToCheck.Length; i++)
        {
            if (tilemap.GetTile(position + positionsToCheck[i]) != null)
            {
                returnList.Add(tilemap.GetTile(position + positionsToCheck[i]) as Tile);
            }
            else
            {
                returnList.Add(null);
            }
        }
        return returnList;
    }

    void AutoGenerateTileObjects(bool clearExistingTiles = true)
    {
        
        charEdgeDict.Clear();
        if (clearExistingTiles) masterTileList.Clear();

        foreach (characterEdgePair pair in characterEdgePairs)
        {
            charEdgeDict.Add(pair.key, pair.value);
        }
        //TileObjectSOs
        //Name of tile.Replace("Tile", "");

        foreach (string dir in exportPaths)
        {
            if (!AssetDatabase.IsValidFolder("Assets/Resources/" + dir + "/TileObjectSOs"))
            {
                AssetDatabase.CreateFolder("Assets/Resources/" + dir, "TileObjectSOs");
            }

            //Iterate over each tile in the original
            string[] files = Directory.GetFiles("Assets/Resources/" + dir + "/Tiles/", "*.asset");
            foreach (string file in files)
            {
                string resourceName = file.Replace("Assets/Resources/", "");
                resourceName = resourceName.Replace(".asset", "");
                Tile tile = Resources.Load<Tile>(resourceName);
                if (tile == null) Debug.Log("Failed to load valid tile");
                TileObject obj = ScriptableObject.CreateInstance("TileObject") as TileObject;
                if (obj == null) Debug.Log("Failed to create valid tile object");
                obj.mapTile = tile;

                resourceName = resourceName.Replace(dir + "/", "");
                resourceName = resourceName.Replace("Tiles/Tile", "");
                TileEdge.Edge[] edges = getEdgesFromName(resourceName);
                obj.SetEdges(edges);

                AssetDatabase.CreateAsset(obj, "Assets/Resources/" + dir + "/TileObjectSOs/" + "TileObject" + resourceName + ".asset");
                masterTileList.Add(obj);
            }
        }
    }

    TileEdge.Edge[] getEdgesFromName(string name)
    {
        TileEdge.Edge[] returnEdges = new TileEdge.Edge[6];

        int i = 0;
        foreach (char letter in name)
        {
            if (i >= returnEdges.Length) break;
            returnEdges[i] = charEdgeDict[letter];
            i++;
        }

        return returnEdges;
    }

    void PopulateTileList()
    {
        foreach (RotatableTile rotatableTile in rotatableTiles)
        {
            TileObject[] rotations = rotatableTile.getTileRotations();
            foreach (TileObject tile in rotations)
            {
                masterTileList.Add(tile);
            }
        }

        foreach (TileObject currentTileObject in masterTileList)
        {

            //For each tileObject that is possible, go over the whole list. If it is compatible for a given position, then 

            List<List<Tile>> tiles = new List<List<Tile>>();
            List<Tile> upperLeftNeighbors = new List<Tile>();
            List<Tile> upperRightNeighbors = new List<Tile>();
            List<Tile> middleRightNeighbors = new List<Tile>();
            List<Tile> lowerRightNeighbors = new List<Tile>();
            List<Tile> lowerLeftNeighbors = new List<Tile>();
            List<Tile> middleLeftNeighbors = new List<Tile>();

            foreach (TileObject possibleNeighbor in masterTileList)
            {
                if (currentTileObject.upperLeftEdge.GetCompatibility(possibleNeighbor.lowerRightEdge.edge))
                {
                    upperLeftNeighbors.Add(possibleNeighbor.mapTile);
                }
                if (currentTileObject.upperRightEdge.GetCompatibility(possibleNeighbor.lowerLeftEdge.edge))
                {
                    upperRightNeighbors.Add(possibleNeighbor.mapTile);
                }
                if (currentTileObject.midRightEdge.GetCompatibility(possibleNeighbor.midLeftEdge.edge))
                {
                    middleRightNeighbors.Add(possibleNeighbor.mapTile);
                }
                if (currentTileObject.lowerRightEdge.GetCompatibility(possibleNeighbor.upperLeftEdge.edge))
                {
                    lowerRightNeighbors.Add(possibleNeighbor.mapTile);
                }
                if (currentTileObject.lowerLeftEdge.GetCompatibility(possibleNeighbor.upperRightEdge.edge))
                {
                    lowerLeftNeighbors.Add(possibleNeighbor.mapTile);
                }
                if (currentTileObject.midLeftEdge.GetCompatibility(possibleNeighbor.midRightEdge.edge))
                {
                    middleLeftNeighbors.Add(possibleNeighbor.mapTile);
                }
            }

            tiles.Add(upperLeftNeighbors);
            tiles.Add(upperRightNeighbors);
            tiles.Add(middleRightNeighbors);
            tiles.Add(lowerRightNeighbors);
            tiles.Add(lowerLeftNeighbors);
            tiles.Add(middleLeftNeighbors);
            adjacencies.Add(currentTileObject.mapTile, tiles);
        }
    }

    public void PopulateTilesForCPC()
    {
        CategorizedProbabilityCalculator catProb = probabilityCalculator as CategorizedProbabilityCalculator;
        if (catProb != null)
        {
            catProb.tileObjects = masterTileList;
        }
        else
        {
            Debug.Log("This functionality only works for a categorized probability calculator, as opposed to all probability calculators.");
        }
    }

    bool isPositionInBounds(Vector3Int positionToCheck)
    {
        return (positionToCheck.x >= constraintMin.x && positionToCheck.x < constraintMax.x && positionToCheck.y >= constraintMin.y && positionToCheck.y < constraintMax.y);
    }

    [CustomEditor(typeof(Generator))]
    public class GeneratorEditor : Editor
    {
        public override void OnInspectorGUI()
        {

            var generator = target as Generator;

            if (GUILayout.Button("Auto-Populate Tiles List"))
            {
                generator.AutoGenerateTileObjects();
            }

            if (GUILayout.Button("Populate Tiles for Probability Calculator"))
            {
                generator.PopulateTilesForCPC();
            }

            base.OnInspectorGUI();
        }
    }
}
