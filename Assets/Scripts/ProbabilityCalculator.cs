using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "ProbabilityCalculator")]
public class ProbabilityCalculator : ScriptableObject
{
    public int defaultWeight = 1;
    public int defaultPriorityLevel = 0;
    //Some sort of system: Given a tile, you can get a list of weights for other neighboring tiles.
    [Serializable]
    public class TileWeightPair
    {
        public Tile tile;
        public int weight;
    }
    [Serializable]
    public class TileWeightEntry
    {
        public Tile keyTile;
        public List<List<TileWeightPair>> pairs;
    }

    public List<TileWeightEntry> tileWeightEntries = new List<TileWeightEntry>();
    protected Dictionary<Tile, List<List<TileWeightPair>>> probabilityDictionary = new Dictionary<Tile, List<List<TileWeightPair>>>();
    public List<int> weights(List<Tile> possibleNeighbors, List<Tile> existingNeighbors)
    {
        if (probabilityDictionary.Count != tileWeightEntries.Count)
        {
            UpdateProbabilityDictionary();
        }
        
        List<int> returnList = new List<int>();
        int priority = 0;

        for (int i = 0; i < possibleNeighbors.Count; i++)
        {
            bool distributionIsDefined = false;
            int calculatedWeight = 0;
            
            foreach (Tile existingNeighbor in existingNeighbors)
            {
                //If the possible option is in the distribution list for the existing neighbor
                if (existingNeighbor != null && probabilityDictionary.ContainsKey(existingNeighbor))
                {
                    foreach (List<TileWeightPair> pairList in probabilityDictionary[existingNeighbor])
                    {
                        if (pairList == null || pairList.Count < 1) continue;
                        foreach (TileWeightPair pair in pairList)
                        {
                            if (pair.tile == possibleNeighbors[i])
                            {
                                distributionIsDefined = true;
                                calculatedWeight += pair.weight;
                            }
                        }
                        if (!distributionIsDefined) continue;
                        break;
                    }
                }
                
            }
            if (distributionIsDefined)
            {
                returnList.Add(calculatedWeight);
            }
            else if (priority == defaultPriorityLevel) //Only add tiles if this is the default priority queue
            {
                returnList.Add(defaultWeight);
            }
            priority++;
        }

        return returnList;
    }

    void UpdateProbabilityDictionary()
    {
        foreach (TileWeightEntry entry in tileWeightEntries)
        {
            if (entry.keyTile != null && probabilityDictionary.ContainsKey(entry.keyTile))
            {
                probabilityDictionary[entry.keyTile] = entry.pairs;
            }
            else if (entry.keyTile != null)
            {
                probabilityDictionary.Add(entry.keyTile, entry.pairs);
            }
        }
    }
}
