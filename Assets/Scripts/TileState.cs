using System.Collections;
using System.Collections.Generic;
using UnityEditor.Tilemaps;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TileState : MonoBehaviour
{
    public bool isCollapsed = false;
    public List<Tile> possibleNeighbors = new List<Tile>();
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool determinePossibleNeighbors(List<List<Tile>> validAdjacentTiles)
    {
        bool errorEncountered = false;

        if (validAdjacentTiles.Count == 0)
        {
            Debug.Log("There are no valid possible neighbors.");
            errorEncountered = true;
            return false;
        }

        possibleNeighbors = new List<Tile>(validAdjacentTiles[0]);

        foreach (List<Tile> adjacencyList in validAdjacentTiles)
        {
            //Look at the current list of possible neighbors. If the current adjacencyList does not contain this tile, remove it from the list of possible neighbors.
            for (int i = possibleNeighbors.Count - 1; i >= 0; i--)
            {
                if (!adjacencyList.Contains(possibleNeighbors[i]))
                {
                    possibleNeighbors.RemoveAt(i);
                }
            }
        }

        if (possibleNeighbors.Count < 1)
        {
            Debug.Log("Error in generating a tile: There were no possible neighbors.");
            errorEncountered = true;
        }
        
        return !errorEncountered;
    }

    public Tile Collapse(List<Tile> existingNeighbors = null, ProbabilityCalculator probabilityCalculator = null)
    {
        if (isCollapsed)
        {
            Debug.Log("Warning: Attempted to collapse a tile that was already collapsed.");
            return null;
        }
        Tile returnTile = null;
        if (possibleNeighbors.Count > 0)
        {
            if (existingNeighbors == null || probabilityCalculator == null)
            {
                returnTile = possibleNeighbors[Random.Range(0, possibleNeighbors.Count)];

                isCollapsed = true;
            }
            else
            {
                List<Tile> weightedPossibleNeighbors = new List<Tile>(); //Pass in possibleNeighbors as well?
                List<int> weights = probabilityCalculator.weights(possibleNeighbors, existingNeighbors);
                for (int i = 0; i < possibleNeighbors.Count; i++)
                {
                    for (int j = 0; j < weights[i]; j++) //Add multiple copies of the possible neighbor based on the weight
                    {
                        weightedPossibleNeighbors.Add(possibleNeighbors[i]);
                    }
                }

                returnTile = weightedPossibleNeighbors[Random.Range(0, weightedPossibleNeighbors.Count)];

                isCollapsed = true;
            }
        }
        

        return returnTile;
    }
}
