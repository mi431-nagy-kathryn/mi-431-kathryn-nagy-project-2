using System.Collections;
using System.Collections.Generic;
using UnityEditor.Timeline.Actions;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName ="TileObject")]
public class TileObject : ScriptableObject
{
    public Tile mapTile;
    public TileEdge upperLeftEdge = new TileEdge();
    public TileEdge upperRightEdge = new TileEdge();
    public TileEdge midLeftEdge = new TileEdge();
    public TileEdge midRightEdge = new TileEdge();
    public TileEdge lowerLeftEdge = new TileEdge();
    public TileEdge lowerRightEdge = new TileEdge();

    public int numberOfEdgesOfType(TileEdge edgeToCompare)
    {
        int returnAmount = 0;
        returnAmount += edgeToCompare.edge == upperLeftEdge.edge ? 1 : 0;
        returnAmount += edgeToCompare.edge == upperRightEdge.edge ? 1 : 0;
        returnAmount += edgeToCompare.edge == midLeftEdge.edge ? 1 : 0;
        returnAmount += edgeToCompare.edge == midRightEdge.edge ? 1 : 0;
        returnAmount += edgeToCompare.edge == lowerLeftEdge.edge ? 1 : 0;
        returnAmount += edgeToCompare.edge == lowerRightEdge.edge ? 1 : 0;
        return returnAmount;
    }

    private void OnEnable()
    {
        
    }

    //The first edge is the upper left, then proceeding clockwise.
    public void SetEdges(TileEdge.Edge[] edges)
    {
        if (edges.Length < 6)
        {
            Debug.Log("The parameter edges passed into SetEdges should have 6 items.");
            return;
        }

        upperLeftEdge.edge = edges[0];
        upperRightEdge.edge = edges[1];
        midRightEdge.edge = edges[2];
        lowerRightEdge.edge = edges[3];
        lowerLeftEdge.edge = edges[4];
        midLeftEdge.edge = edges[5];
    }
}
