using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

public class RotatableTile : ScriptableObject
{
    //Given a tile object, this should generate more tile objects that represent different rotations.
    // Should the tile objects still be scriptable objects? I'm not 100% sure

    public TileObject initialTile;
    public Tile[] replacementTiles = { };
    TileObject[] tileRotations = { };
    int maxTileVariations = 6;

    public void GenerateTileRotations()
    {
        TileEdge[] originalEdges = { initialTile.upperLeftEdge, initialTile.upperRightEdge, initialTile.midRightEdge, initialTile.lowerRightEdge, initialTile.lowerLeftEdge, initialTile.midLeftEdge };
        for (int i = 0; i < maxTileVariations; i++)
        {
            TileObject tileToAdd = new TileObject();
            //Add a check for duplicates - this should only exist for solid tiles and ones with alternating edges, in the case of hex tiles
            tileToAdd.upperLeftEdge = originalEdges[(i + 0) % 6];
            tileToAdd.upperRightEdge = originalEdges[(i + 1) % 6];
            tileToAdd.midRightEdge = originalEdges[(i + 2) % 6];
            tileToAdd.lowerRightEdge = originalEdges[(i + 3) % 6];
            tileToAdd.lowerLeftEdge = originalEdges[(i + 4) % 6];
            tileToAdd.midLeftEdge = originalEdges[(i + 5) % 6];
            if (replacementTiles.Length > i && replacementTiles[i] != null)
            {
                tileToAdd.mapTile = replacementTiles[i];
            }
            else
            {
                tileToAdd.mapTile = initialTile.mapTile;
            }
            tileRotations.Append(tileToAdd); //Is there a reason I didn't add this in my first pass through?
        }
    }

    public TileObject[] getTileRotations() { return tileRotations; }
}
