using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TileEdge
{
    public enum Edge
    {
        Grass,
        Water
    }

    public Edge edge;

    //Something about the compatibility between edges
    public bool GetCompatibility(Edge edgeToCheck)
    {
        return edge == edgeToCheck; //A more sophisticated algorithm can be implemented later
    }
}
